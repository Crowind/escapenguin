﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SealScript : MonoBehaviour
{
	public float speed;
	private Vector3 start;
	public GameObject arrivePoint;
	private Transform end;
	private SpriteRenderer spRend;

	private bool go = true;

	// Start is called before the first frame update
	void Start()
    {
		spRend = GetComponent<SpriteRenderer>();
		start = transform.position;
		end = arrivePoint.transform;
    }

    // Update is called once per frame
    void Update()
    {
		if (go) {
			transform.position = Vector3.MoveTowards(transform.position, end.position, speed);
		}
		else {
			transform.position = Vector3.MoveTowards(transform.position, start, speed);
		}

		if (transform.position == end.position) {
			go = false;
			spRend.flipX = true;
			
		}

		if (transform.position == start) {

			spRend.flipX = false;
			go = true;
		}



	}
}
