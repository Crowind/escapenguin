﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Start_button_script : MonoBehaviour
{

    void Start() {
        GetComponent<Button>().onClick.AddListener(delegate() { Play(); });
    }

    void Play() {
        SceneManager.LoadScene("GameScene");
    }
}