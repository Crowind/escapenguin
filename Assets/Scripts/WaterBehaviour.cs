﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterBehaviour : MonoBehaviour {

	public int respawnTimeWhale=2;
	private float lastSpawn;
	public GameObject spawn;
	public GameObject blood;

	// Start is called before the first frame update
	void Start() {
		lastSpawn = Time.time;

	}

	// Update is called once per frame
	void Update() {

	}

	private void OnCollisionEnter2D(Collision2D collision) {
		if (collision.gameObject.tag == "Penguin" && lastSpawn + respawnTimeWhale < Time.time) {

			lastSpawn = Time.time;

			ContactPoint2D cPoint = collision.GetContact(0);
			Vector2 norm= collision.GetContact(0).normal;
			Vector2 otherPos = cPoint.otherCollider.transform.position;

			float angle = 0 ;

			if (norm.x == 1) {
				angle = 90;
			}
			if (norm.y == 1) {
				angle =180;
			}
			if (norm.x == -1) {
				angle = -90;
			}
			if (norm.y == -1) {
				angle = 0;
			}


			GameObject orca = Instantiate(spawn, cPoint.point, Quaternion.identity);

			orca.transform.Rotate(Vector3.forward,angle);

			GameObject bl=Instantiate(blood, cPoint.point, Quaternion.identity);

			bl.transform.position = new Vector3(bl.transform.position.x, bl.transform.position.y, -2);

			Destroy(collision.gameObject);

		}
	}
}
