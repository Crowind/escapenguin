﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Random_Penguin_script : MonoBehaviour
{
    private Rigidbody2D rb;
	private Vector3 target;
    public float forceAmount=10;
    public float max_velocity=.25f;
	public float noMoveRadius=.03f;
    private SpriteRenderer sprite;
    private Animator animator;

	public float deltaTime = 5;
	private float lastMove;

    void Start() {
		lastMove = Time.time-10;
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }

    void Update() {
        // Move penguins randomly

		
	
		if (Time.time>lastMove+deltaTime+Random.Range(-1,4)) {

			target = new Vector3(transform.position.x + Random.Range(-100, 100), transform.position.y + Random.Range(-100, 100), 0);
			rb.velocity = Vector2.zero;
			rb.angularDrag = 0;
            rb.AddForce((target - transform.position).normalized * forceAmount);
			lastMove = Time.time;
		}
	

        if (rb.velocity.magnitude > max_velocity)
			rb.velocity = rb.velocity.normalized * max_velocity + (new Vector2(Random.Range(-.5f, .5f), Random.Range(-.5f, .5f)));

        //Flip the sprite when he goes left
        float h_velocity = target.x - transform.position.x;
        float v_velocity = target.y - transform.position.y;

        if (h_velocity < 0)
            sprite.flipX = true;
        else
            sprite.flipX = false;

        // Tell animation player y velocity
        animator.SetFloat("Velocity_y", v_velocity);
    }
}