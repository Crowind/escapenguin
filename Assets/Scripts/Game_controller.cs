﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game_controller : MonoBehaviour
{
    private int n_penguins;
	public Camera cam;
    public TextMeshProUGUI n_penguins_text;
    public GameObject  retry_button;
	public GameObject albatros;

    // Start is called before the first frame update
    void Start()
    {
        retry_button.GetComponent<Button>().onClick.AddListener(delegate() { retry(); });
    }

    // Update is called once per frame
    void Update()
    {
        n_penguins = GameObject.FindGameObjectsWithTag("Penguin").Length;
        n_penguins_text.text = n_penguins.ToString();
        if (n_penguins == 0)
            gameOver();
    }

    void gameOver() {
        retry_button.SetActive(true);
		cam.GetComponent<CameraBehaviour>().Stop();
		Cursor.visible = true;
		albatros.SetActive(false);

    }

    void retry() {
        SceneManager.LoadScene("MainMenu");
		retry_button.SetActive(false);
    }
}
