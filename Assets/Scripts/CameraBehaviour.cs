﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
	public float speed =0.5f;
		
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		transform.position = new Vector3 (transform.position.x,transform.position.y + speed * Time.deltaTime ,transform.position.z);
    }

	void OnTriggerExit2D(Collider2D other) {
		Destroy(other.gameObject);
	}

	public void Stop() {
		speed = 0;
	}
}

