﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WinScript : MonoBehaviour
{
	public GameObject winText;
	void OnTriggerEnter2D(Collider2D collision) {
		

		if (collision.gameObject.CompareTag("Penguin")) {


			GameObject[] array=GameObject.FindGameObjectsWithTag("Penguin");

			foreach ( GameObject pen in array) {

				pen.GetComponent<Penguin_script>().Arrive();
			}

			Camera.main.GetComponent<CameraBehaviour>().speed = 0;
			winText.SetActive(true);
			
		}
	}

}
