﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlbatrosBehaviour : MonoBehaviour {

	private Vector3 target;
	private SpriteRenderer sprite;
	private Animator animator;
	public float speed=100;

	private Vector2 old_position;

	// Start is called before the first frame update
	void Start() {
		old_position = Vector2.zero;
		Cursor.visible = false;
		sprite = GetComponent<SpriteRenderer>();
		animator = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update() {
		// Move penguins where the mouse is

		target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		

		//Flip the sprite when he goes left
		float h_velocity = Input.mousePosition.x - old_position.x;
		float v_velocity = Input.mousePosition.y - old_position.y;

		if (Vector3.Distance(old_position , Input.mousePosition)>5 ){

			old_position = Input.mousePosition;

		}

		if (h_velocity != 0) {
			if (h_velocity > 0 )
				sprite.flipX = true;
			else
				sprite.flipX = false;
		}

		transform.position = target + Vector3.forward*10;

		// Tell animation player y velocity
		animator.SetFloat("Velocity_y", v_velocity);
	}
}
