﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinMoveScript : MonoBehaviour
{
	public float movement_speed;
	public float movement_amount;
	private float initial_x;
	private float initial_y;

	void Start() {
		initial_x = transform.position.x;
		initial_y = transform.position.y;
	}

	void Update() {
		transform.position = new Vector2(initial_x, initial_y + movement_amount * Mathf.Sin(Time.time * movement_speed));
	}
}
