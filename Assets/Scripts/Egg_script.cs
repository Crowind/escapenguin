﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Egg_script : MonoBehaviour
{
    private Rigidbody rb;
    private Animator animator;

	public GameObject particle;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Penguin")) {
            StartCoroutine(Hatch(other));
            animator.SetBool("Hatching", true);
			particle.SetActive(true);
        }
    }

    IEnumerator Hatch(Collider2D other) {
        yield return new WaitForSeconds(1);
        Instantiate(other, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

}
